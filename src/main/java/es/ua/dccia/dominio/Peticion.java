/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.dominio;

import es.ua.dccia.rest.util.MiDateAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.validation.constraints.Future;
import javax.validation.constraints.Past;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author otto
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Peticion.findDestacadas",
            query = "SELECT p FROM Peticion p WHERE p.destacada=TRUE ORDER BY p.inicio DESC")
})
public class Peticion implements Serializable {

    @XmlID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @NotBlank
    String titulo;
    //para que asocie un campo con longitud mayor de 255 en la BD
    //@Column(columnDefinition = "text")
    @NotBlank        
    String texto;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Past        
    Date inicio;
    @Future
    @Temporal(javax.persistence.TemporalType.DATE)
    @XmlJavaTypeAdapter(MiDateAdapter.class)
    Date fin;
    long firmasObjetivo;
    long firmasConseguidas;
    boolean abierta;
    boolean conseguida;
    boolean destacada;
    @ManyToOne
    @XmlIDREF
    Usuario creador;
    @XmlIDREF
    @OneToMany(mappedBy = "peticion", fetch = FetchType.LAZY)
    Set<Firma> firmas;
    @XmlIDREF
    @OneToMany(mappedBy = "peticion", fetch = FetchType.LAZY)
    private Set<Actualizacion> actualizaciones;
    @XmlIDREF
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Destinatario> destinatarios;

    public Peticion() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        
        if (texto.contains("\n")) {
            String textoParrafos = texto.replace("\n", "</p><p>");
            return "<p>" +  textoParrafos + "</p>";
        }
        else
            return "<p>" +  texto + "</p>";
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fechaFin) {
        this.fin = fechaFin;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public long getFirmasObjetivo() {
        return firmasObjetivo;
    }

    public void setFirmasObjetivo(long firmasObjetivo) {
        this.firmasObjetivo = firmasObjetivo;
    }

    public long getFirmasConseguidas() {
        return firmasConseguidas;
    }

    public void setFirmasConseguidas(long firmasConseguidas) {
        this.firmasConseguidas = firmasConseguidas;
    }

    public boolean isAbierta() {
        return abierta;
    }

    public void setAbierta(boolean abierta) {
        this.abierta = abierta;
    }

    public boolean isConseguida() {
        return conseguida;
    }

    public void setConseguida(boolean conseguida) {
        this.conseguida = conseguida;
    }

    public boolean isDestacada() {
        return destacada;
    }

    public void setDestacada(boolean destacada) {
        this.destacada = destacada;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void setCreador(Usuario creador) {
        this.creador = creador;
    }

    public Set<Firma> getFirmas() {
        return firmas;
    }

    public void setFirmas(Set<Firma> firmas) {
        this.firmas = firmas;
    }

    public Set<Actualizacion> getActualizaciones() {
        return actualizaciones;
    }

    public void setActualizaciones(Set<Actualizacion> actualizaciones) {
        this.actualizaciones = actualizaciones;
    }

    public Set<Destinatario> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(Set<Destinatario> destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    @Override
    public String toString() {
        return id +  " titulo: " +  titulo + "/inicio: "+ inicio + "/fin: " + fin + "/conseguidas: " + firmasConseguidas + 
                "/objetivo: " + firmasObjetivo + "/abierta:" + abierta + "/destacada:" + destacada + "/conseguida:" + conseguida + "/texto: " + texto;
    }
}
