/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author otto
 */
@Entity
public class Denuncia implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @Temporal(TemporalType.TIMESTAMP)
    Date fecha;
    String texto;
    
    @ManyToOne
    Usuario denunciante;
}
