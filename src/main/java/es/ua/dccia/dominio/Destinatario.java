/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.dominio;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlID;

/**
 *
 * @author otto
 */
@Entity
public class Destinatario implements Serializable {

    @Id
    @XmlID
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nombre;
    private String cargo;
    @ManyToMany(mappedBy = "destinatarios")
    private Set<Peticion> peticiones;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Set<Peticion> getPeticiones() {
        return peticiones;
    }

    public void setPeticiones(Set<Peticion> peticiones) {
        this.peticiones = peticiones;
    }
}
