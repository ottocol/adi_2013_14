/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author otto
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Actualizacion.listarUltimas",
            query = "select a from Actualizacion a where a.peticion.id=:idPeticion order by a.fecha desc"
    )
})
public class Actualizacion implements Serializable {

    @Id
    @XmlID
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fecha;
    @NotBlank
    private String contenido;
    @XmlIDREF
    @ManyToOne
    Peticion peticion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Peticion getPeticion() {
        return peticion;
    }

    public void setPeticion(Peticion peticion) {
        this.peticion = peticion;
    }
}
