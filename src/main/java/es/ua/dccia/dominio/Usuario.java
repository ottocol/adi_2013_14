/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.dominio;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlID;
import org.eclipse.persistence.oxm.annotations.XmlReadOnly;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author otto
 */
@Entity
public class Usuario implements Serializable {

    @Id
    @XmlID
    @NotBlank(message = "El login no puede estar vacío")
    @Email(message = "El login debe ser una dirección de email válida")
    private String login;
    //Feature de Moxy por la que la propiedad se puede deserializar pero no se va a serializar
    @XmlReadOnly
    @NotBlank(message = "La contraseña no puede estar vacía")
    private String password;
    @NotBlank(message = "El nombre no puede estar vacío")
    private String nombre;
    @NotBlank(message = "Los apellidos no pueden estar vacíos")
    private String apellidos;
    private String rol;
    @OneToMany(mappedBy = "creador")
    private Set<Peticion> peticiones;
    @OneToMany(mappedBy = "usuario")
    private Set<FirmaAutentificada> firmas;

    public Usuario() {
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Set<Peticion> getPeticiones() {
        return peticiones;
    }

    public void setPeticiones(Set<Peticion> peticiones) {
        this.peticiones = peticiones;
    }

    public Set<FirmaAutentificada> getFirmas() {
        return firmas;
    }

    public void setFirmas(Set<FirmaAutentificada> firmas) {
        this.firmas = firmas;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    @Override
    public String toString() {
        return this.login + "/" + this.password + "/" + this.apellidos + "," + "/" + this.rol;
    }
    
}
