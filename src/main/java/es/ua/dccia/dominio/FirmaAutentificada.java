package es.ua.dccia.dominio;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlIDREF;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author otto
 */
@Entity
@DiscriminatorValue("autentificada")
public class FirmaAutentificada extends Firma {

    @XmlIDREF
    @ManyToOne           
    Usuario usuario;
    
    public FirmaAutentificada() {
        super();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
