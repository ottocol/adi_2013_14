/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author otto
 */
public class Utils {

    /* En lugar de usar BeanUtils, vamos a tomar prestado este código
    /* http://stackoverflow.com/questions/835416/how-to-copy-properties-from-one-java-bean-to-another
    /* usar http://mapstruct.org?
    */
    public static void copyProperties(Object fromObj, Object toObj, boolean copyNulls) {
        Class<? extends Object> fromClass = fromObj.getClass();
        Class<? extends Object> toClass = toObj.getClass();

        try {
            BeanInfo fromBean = Introspector.getBeanInfo(fromClass);
            BeanInfo toBean = Introspector.getBeanInfo(toClass);

            PropertyDescriptor[] toPd = toBean.getPropertyDescriptors();
            List<PropertyDescriptor> fromPd = Arrays.asList(fromBean
                    .getPropertyDescriptors());

            for (PropertyDescriptor propertyDescriptor : toPd) {
                propertyDescriptor.getDisplayName();
                PropertyDescriptor pd = fromPd.get(fromPd
                        .indexOf(propertyDescriptor));
                if (pd.getDisplayName().equals(
                        propertyDescriptor.getDisplayName())
                        && !pd.getDisplayName().equals("class")) {
                    if (propertyDescriptor.getWriteMethod() != null) {
                        Object propFrom = pd.getReadMethod().invoke(fromObj, null);
                        if (copyNulls || propFrom!=null)
                            propertyDescriptor.getWriteMethod().invoke(toObj, propFrom);
                    }
                }

            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
