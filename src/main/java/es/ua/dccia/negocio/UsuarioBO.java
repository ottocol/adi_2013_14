/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.negocio;

import es.ua.dccia.MueveteException;
import es.ua.dccia.datos.PersistenceManager;
import es.ua.dccia.datos.UsuarioDao;
import es.ua.dccia.dominio.Usuario;
import es.ua.dccia.rest.to.UsuarioTO;
import javax.persistence.EntityManager;

/**
 *
 * @author otto
 */
public class UsuarioBO {

    public Usuario getUsuario(String login) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        UsuarioDao uDao = new UsuarioDao(em);
        Usuario usuario = uDao.find(login);
        //for (Peticion p: usuario.getPeticiones()) {
        //    p.getId();
        //}
        em.getTransaction().commit();
        em.close();
        return usuario;

    }

    public void registrar(Usuario u) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        try {
            em.getTransaction().begin();
            if (this.getUsuario(u.getLogin()) == null) {
                UsuarioDao uDao = new UsuarioDao(em);
                uDao.create(u);
                //copiar imagen de perfil por defecto con el login del usuario
                ImagenService.getInstance().copiaPerfilDefecto(u.getLogin());
            } else {
                throw new MueveteException("El usuario " + u.getLogin() + " ya existe");
            }
        } finally {
            em.getTransaction().commit();
            em.close();
        }
    }

    public void modificar(UsuarioTO uto) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        Usuario encontrado = em.find(Usuario.class, uto.getLogin());
        copiarUsuario(uto, encontrado);
        em.getTransaction().commit();
        em.close();
    }
    
    private void copiarUsuario(UsuarioTO origen, Usuario destino) {
        String nombre = origen.getNombre();
        String apellidos = origen.getApellidos();
        String password = origen.getNewPassword();
        if (nombre!=null)
            destino.setNombre(nombre);
        if (apellidos!=null)
            destino.setApellidos(apellidos);
        if (password!=null)
            destino.setPassword(password);       
    }
}


