package es.ua.dccia.negocio;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import javax.imageio.ImageIO;

import es.ua.dccia.ImageUploadException;

public class ImagenService implements IImagenService{

	private static final long MAX_FILE_SIZE = 100000000;
        private static ImagenService imagenService = null;
        private String appFilePath;
        
        
        public static ImagenService getInstance() {
            if (imagenService == null)
                imagenService = new ImagenService();
            return imagenService;
        }

        private ImagenService() {
            
        }
      

        @Override
	public void guardaThumbnail(InputStream imag, String path, String nomFich,
			int anchoThumb) {
		try {
			BufferedImage imagen = ImageIO.read(imag);
			if (imagen == null) {
				throw new ImageUploadException("No es una imagen o el formato es incorrecto");
			}
			// calculamos el alto del thumbnail. Hay que mantener las
			// proporciones del original
			double anchoOriginal = (double) imagen.getWidth();
			double altoOriginal = (double) imagen.getHeight();
			double ratioOriginal = anchoOriginal / altoOriginal;
			int altoThumb = (int) (anchoThumb / ratioOriginal);
			// creamos el thumbnail
			BufferedImage thumb = new BufferedImage(anchoThumb, altoThumb, imagen.getType());
			// dibujamos en �l la imagen escalada
			Graphics2D graphics2D = thumb.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(imagen, 0, 0, anchoThumb, altoThumb, null);
			// guardar thumbnail en formato JPG
			File f = new File(this.appFilePath + path +  nomFich + "."	+ DEFAULT_FORMAT);
			ImageIO.write(thumb, DEFAULT_FORMAT, f);
		} catch (IOException ioe) {
			throw new ImageUploadException(ioe);
		}
	}
	
        @Override
	public void guardaFotoYThumbnail(InputStream imagen, String path, String nomFich, int anchoThumb) {
		try {
                        //La imagen original se copia
			File f = new File(this.appFilePath + path + nomFich + "_o." + DEFAULT_FORMAT);
			FileOutputStream fos = new FileOutputStream(f);
			FileChannel canalDestino = fos.getChannel();
			ReadableByteChannel canalOrigen = Channels.newChannel(imagen);
			canalDestino.transferFrom(canalOrigen,0, MAX_FILE_SIZE);
			canalDestino.close();
			fos.close();
                        //El thumbnail hay que escalarlo, por eso el proceso es distinto
			guardaThumbnail(new FileInputStream(f), path, nomFich, anchoThumb);
		} catch (FileNotFoundException e) {
			throw new ImageUploadException("No se puede crear el archivo con la imagen");
		} catch (IOException e) {
			throw new ImageUploadException("Problema al intentar operar con el fichero de imagen");
		}
	}

        @Override
	public void copiaPerfilDefecto(String nombre) {
		FileChannel canalOrig;
		try {
			FileInputStream fis = new FileInputStream(this.appFilePath +  DEFAULT_PROFILE);
			canalOrig = fis.getChannel();
			FileOutputStream fos = new FileOutputStream(this.appFilePath 
					+ PROFILE_FOLDER +  nombre + "." + DEFAULT_FORMAT);
			FileChannel canalDest = fos.getChannel();
			canalDest.transferFrom(canalOrig, 0, canalOrig.size());
			fis.close();
			fos.close();
		} catch (FileNotFoundException fnfe) {
			throw new ImageUploadException(fnfe);
		} catch (IOException ioe) {
			throw new ImageUploadException(ioe);
		}

	}

    @Override
    public void setAppFilePath(String path) {
        if (path.charAt(path.length()-1)!='/')
            this.appFilePath = path + "/";
        else
            this.appFilePath = path;   
    }

    @Override
    public String getAppFilePath() {
        return this.appFilePath;
    }
}