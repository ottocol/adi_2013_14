package es.ua.dccia.negocio;

import java.io.InputStream;

public interface IImagenService {
	
	public static final int THUMB_PROFILE_WIDTH = 80;
	public static final int THUMB_PHOTO_WIDTH = 300;
	public static final String DEFAULT_FORMAT = "jpg";
	public static final String DEFAULT_PROFILE = "img/perfiles/default/default.jpg";
	public static final String PROFILE_FOLDER = "img/perfiles/";
	public static final String PHOTOS_FOLDER = "img/peticiones/";

    public void guardaThumbnail(InputStream im, String path, String nomFich, int anchoThumb);
    public void guardaFotoYThumbnail(InputStream imagPerfil, String path, String nomFich, int anchoThumb);
    public void copiaPerfilDefecto(String nombre);
    public void setAppFilePath(String path);
    public String getAppFilePath();
}