/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.negocio;

import es.ua.dccia.AccesoNoPermitidoException;
import es.ua.dccia.EntidadNoEncontradaException;
import es.ua.dccia.MueveteException;
import es.ua.dccia.datos.FirmaDao;
import es.ua.dccia.datos.PersistenceManager;
import es.ua.dccia.datos.PeticionDao;
import es.ua.dccia.datos.UsuarioDao;
import es.ua.dccia.dominio.Destinatario;
import es.ua.dccia.dominio.Firma;
import es.ua.dccia.dominio.FirmaAutentificada;
import es.ua.dccia.dominio.FirmaSinAutentificar;
import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.rest.to.FirmaTO;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author otto
 */
public class PeticionBO {

    public List<Peticion> listarDestacadas(int cantidad) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        PeticionDao pdao = new PeticionDao(em);
        List<Peticion> destacadas = pdao.findDestacadas(cantidad);
        //queremos que los datos del creador sean accesibles, aunque parece que los @ManyToOne sí lo son por defecto
        for (Peticion p : destacadas) {
            p.getCreador();
        }
        em.getTransaction().commit();
        em.close();
        return destacadas;
    }

    public Peticion getPeticion(long id) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        PeticionDao pdao = new PeticionDao(em);
        Peticion peticion = pdao.find(id);
        if (peticion!=null)
            for (Destinatario destinatario : peticion.getDestinatarios()) {
                destinatario.getNombre();
            }
        em.getTransaction().commit();
        em.close();
        return peticion;
    }

    public long crearPeticion(Peticion peticion, String loginCreador) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        PeticionDao pdao = new PeticionDao(em);
        //poner los valores por defecto
        peticion.setAbierta(true);
        peticion.setConseguida(false);
        peticion.setDestacada(false);
        peticion.setInicio(new Date());
        peticion.setFirmasConseguidas(0);
        //guardar la petición en la BD y asignarla al usuario actual
        pdao.create(peticion);
        UsuarioDao udao = new UsuarioDao(em);
        peticion.setCreador(udao.find(loginCreador));
        em.getTransaction().commit();
        em.close();
        return peticion.getId();
    }

    public long firmarPeticion(long idPeticion, FirmaTO firmaTO, String autentificado) {
        Firma firma;
        
        
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        
        //comprobar que la petición existe
        PeticionDao peticionDao = new PeticionDao(em);
        Peticion peticion = peticionDao.find(idPeticion);
        if (peticion==null)
            throw new MueveteException("La petición que estás intentando firmar no existe: " + idPeticion);   
        //lo hacemos distinto según si es una firma autentificada o no
        if (autentificado != null) {
            firma = new FirmaAutentificada();
            FirmaAutentificada fa = (FirmaAutentificada) firma;
            UsuarioDao udao = new UsuarioDao(em);
            fa.setUsuario(udao.find(autentificado));
        }
        else {
            firma = new FirmaSinAutentificar();
            FirmaSinAutentificar fsa = (FirmaSinAutentificar) firma;
            fsa.setNombre(firmaTO.getNombre());
            fsa.setApellidos(firmaTO.getApellidos());
            fsa.setEmail(firmaTO.getEmail());
        }
        //atributos comunes
        firma.setComentario(firmaTO.getComentario());
        firma.setPeticion(peticion);
        firma.setPublica(firmaTO.isPublica());
        
        //actualizar campos de relación y otros
        peticion.getFirmas().add(firma);
        peticion.setFirmasConseguidas(peticion.getFirmasConseguidas()+1);
        new FirmaDao(em).create(firma);
        em.getTransaction().commit();
        em.close();
        return firma.getId();
    }
    
    public Firma getFirma(long idFirma) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        Firma firma = new FirmaDao(em).find(idFirma);
        if (firma!=null)
            if (firma.isPublica())
                return firma;
            else throw new AccesoNoPermitidoException("La firma no es pública");
        else throw new EntidadNoEncontradaException("No existe firma con el id " + idFirma);
    }
}
