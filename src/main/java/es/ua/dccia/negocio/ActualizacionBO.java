/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.negocio;

import es.ua.dccia.EntidadNoEncontradaException;
import es.ua.dccia.datos.ActualizacionDao;
import es.ua.dccia.datos.PersistenceManager;
import es.ua.dccia.datos.PeticionDao;
import es.ua.dccia.dominio.Actualizacion;
import es.ua.dccia.dominio.Peticion;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author otto
 */
public class ActualizacionBO {

    //cuántas actualizaciones se listarán por defecto
    public static int DEF_ULTIMAS = 5;

    public List<Actualizacion> listarUltimas(long idPeticion) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        List<Actualizacion> ultimas = new ActualizacionDao(em).listarUltimas(idPeticion, 0);
        em.getTransaction().commit();
        em.close();
        return ultimas;
    }
        
    public long crearActualizacion(Actualizacion actualizacion, long idPeticion) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        ActualizacionDao adao = new ActualizacionDao(em);
        actualizacion.setFecha(new Date());
        PeticionDao pdao = new PeticionDao(em);
        Peticion peticion = pdao.find(idPeticion);
        if (peticion==null)
            throw new EntidadNoEncontradaException("No existe la petición con id: " + idPeticion);
        actualizacion.setPeticion(peticion);
        adao.create(actualizacion);
        em.getTransaction().commit();
        em.close();        
        return actualizacion.getId();
    }
    
    public void modificarActualizacion(Actualizacion actualizacion) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        ActualizacionDao adao = new ActualizacionDao(em);
        Actualizacion buscada = adao.find(actualizacion.getId());
        buscada.setFecha(new Date());
        buscada.setContenido(actualizacion.getContenido());
        adao.update(buscada);
        em.getTransaction().commit();
        em.close();        
    }
    
    public void borrarActualizacion(long idActualizacion) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        ActualizacionDao adao = new ActualizacionDao(em);
        Actualizacion actualizacion = adao.find(idActualizacion);
        if (actualizacion==null)
            throw new EntidadNoEncontradaException("No existe la actualización con id: " + idActualizacion);
        adao.delete(actualizacion);
        em.getTransaction().commit();
        em.close();        
    }
}
