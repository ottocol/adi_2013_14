/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.MueveteException;
import es.ua.dccia.Tokens;
import es.ua.dccia.dominio.Usuario;
import es.ua.dccia.negocio.UsuarioBO;
import es.ua.dccia.rest.to.UsuarioTO;
import es.ua.dccia.utils.Utils;
import java.net.URI;
import java.util.Map;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author otto
 */
@Path("/usuarios")
public class UsuarioResource {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registrar(@Valid Usuario u, @Context UriInfo uriInfo) {
        UsuarioBO usuarioBO = new UsuarioBO();
        try { 
            u.setRol(Tokens.DEFAULT_USER_ROLE);
            usuarioBO.registrar(u);
        } catch(MueveteException me) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Error al crear usuario").build();
        }
        URI uri = uriInfo.getAbsolutePathBuilder().path("{login}").build(u.getLogin());    
        return Response.created(uri).build();
    }
    
    @Path("{login}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)    
    public Usuario getUsuario(@PathParam("login") String login) {
        Usuario buscado = new UsuarioBO().getUsuario(login);
        if (buscado!=null) {
            buscado.setFirmas(null);
            buscado.setPeticiones(null);
            return buscado;
        }
        else
            throw new CustomNotFoundException("Usuario no encontrado");
                
    }
    
    @Path("{login}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)  
    @RolesAllowed("registrado")
    public String actualizaUsuario(@PathParam("login") String login, UsuarioTO uto, @Context SecurityContext sc) {
        //comprobar que se intenta modificar el usuario logueado actualmente
        if (!login.equals(sc.getUserPrincipal().getName()))
            throw new CustomWebAppException("No puedes modificar los datos de otro usuario", Response.Status.FORBIDDEN);
        UsuarioBO ubo = new UsuarioBO();
        //Si estamos intentando cambiar el password, comprobar que el usuario sabe su password antiguo
        if (uto.getNewPassword()!=null && !ubo.getUsuario(login).getPassword().equals(uto.getOldPassword()))
           throw new CustomWebAppException("La antigua contraseña no es correcta", Response.Status.FORBIDDEN);
        ubo.modificar(uto);
        return "OK";
    }
    
}
