/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.AccesoNoPermitidoException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author otto
 */
@Provider
public class AccesoNoPermitidoMapper implements ExceptionMapper<AccesoNoPermitidoException> {
  @Override
  public Response toResponse(AccesoNoPermitidoException ex) {
    return Response.status(403).
      entity(ex.getMessage()).
      type("text/plain").
      build();
  }
}
