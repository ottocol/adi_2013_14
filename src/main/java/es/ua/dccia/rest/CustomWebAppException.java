/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class CustomWebAppException extends WebApplicationException {
 
  /**
  * Create a HTTP 404 (Not Found) exception.
  */
  public CustomWebAppException(Response.Status status) {
    super(status);
  }
 
  /**
  * Create a HTTP 404 (Not Found) exception.
  * @param message the String that is the entity of the 404 response.
  */
  public CustomWebAppException(String message, Response.Status status) {
    super(Response.status(status)
       .entity(message).type("text/plain").build());
  }
}
