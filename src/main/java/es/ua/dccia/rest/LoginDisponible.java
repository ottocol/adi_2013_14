/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.negocio.UsuarioBO;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author otto
 */
@Path("/loginDisponible/{login}")
public class LoginDisponible {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String loginDisponible(@PathParam("login") String login) {
        UsuarioBO usuarioBO = new UsuarioBO();
        if (usuarioBO.getUsuario(login)==null)
            return "OK";
        else
            return "no";
    }
    
}
