/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.rest;

import es.ua.dccia.dominio.Usuario;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author otto
 */
@Path("saludo")
public class SaludoResource {

    @GET
    @Produces("text/plain")
    public String saludar(@Context ServletContext servletContext ,@Context SecurityContext sc, @Context HttpServletRequest request) {
        System.out.println("Context path:" + servletContext.getContextPath());
        System.out.println("Autentificado como:" + sc.getUserPrincipal());
        System.out.println("Autentificado como:" + request.getUserPrincipal());
        
        return "hola JAX-RS 2.0!!";
    }

    @Path("usuario")
    @GET
    @Produces("application/json")
    public Usuario getUsuario() {
        Usuario u = new Usuario();
        u.setLogin("ejemplo");
        u.setPassword("secreto");
        return u;
    }
}
