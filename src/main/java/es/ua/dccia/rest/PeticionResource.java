/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.Tokens;
import es.ua.dccia.dominio.Firma;
import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.negocio.PeticionBO;
import es.ua.dccia.rest.to.FirmaTO;
import java.net.URI;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author otto
 */
@Path("/peticiones")
public class PeticionResource {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(Tokens.DEFAULT_USER_ROLE)
    public Response crearPeticion(@Valid Peticion peticion, @Context SecurityContext sc, @Context UriInfo uriInfo) {
       long idPeticion = new PeticionBO().crearPeticion(peticion, sc.getUserPrincipal().getName());
       URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(idPeticion);    
       return Response.created(uri).build();
    }
    
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Peticion getPeticion(@PathParam("id") long id) {
        PeticionBO peticionBO = new PeticionBO();
        Peticion peticion = peticionBO.getPeticion(id);
        if (peticion!=null) {
            peticion.setActualizaciones(null);
            peticion.setFirmas(null);
            peticion.setDestinatarios(null);
            //peticion.setCreador(null);
            return peticion;
        }
        else
            throw new CustomNotFoundException("La petición con el id '" + id +"' no existe");
    }
    
    @Path("{id}/firmas")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response firmarPeticion(@PathParam("id") long idPeticion, FirmaTO firmaTO, @Context SecurityContext sc, @Context UriInfo uriInfo) {
        String autentificado = null;
        if (sc.getUserPrincipal()!=null)
            autentificado = sc.getUserPrincipal().getName();
        PeticionBO peticionBO = new PeticionBO();
        long idFirma = peticionBO.firmarPeticion(idPeticion, firmaTO, autentificado);
        URI uri = uriInfo.getAbsolutePathBuilder().path("{idFirma}").build(idFirma);    
        return Response.created(uri).build();
    }
    
    @Path("{idPeticion}/firmas/{idFirma}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Firma obtenerFirma(@PathParam("idPeticion") long idPeticion, @PathParam("idFirma") long idFirma) {
        return new PeticionBO().getFirma(idFirma);
    }
}
