/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.rest;

import es.ua.dccia.Tokens;
import es.ua.dccia.dominio.Actualizacion;
import es.ua.dccia.negocio.ActualizacionBO;
import java.net.URI;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author otto
 */
@Path("/peticiones/{idPeticion}/actualizaciones")

public class ActualizacionResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Actualizacion> listarUltimas(@PathParam("idPeticion") long idPeticion) {
        ActualizacionBO abo = new ActualizacionBO();
        return abo.listarUltimas(idPeticion);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(Tokens.DEFAULT_USER_ROLE)
    public Response crear(@Valid Actualizacion actualizacion, @PathParam("idPeticion") long idPeticion, @Context UriInfo uriInfo) {
        long idNueva = new ActualizacionBO().crearActualizacion(actualizacion, idPeticion);
        URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(idNueva);
        return Response.created(uri).entity(actualizacion).build();
    }
    
    @DELETE
    @Path("{idActualizacion}")
    @RolesAllowed(Tokens.DEFAULT_USER_ROLE)
    public Response eliminar(@PathParam("idActualizacion") long idActualizacion) {
        new ActualizacionBO().borrarActualizacion(idActualizacion);
        return Response.ok().build();
    }
    
    @PUT
    @Path("{idActualizacion}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(Tokens.DEFAULT_USER_ROLE)
    public Response modificar(@PathParam("idActualizacion") long idActualizacion, Actualizacion actualizacion) {
        actualizacion.setId(idActualizacion);
        new ActualizacionBO().modificarActualizacion(actualizacion);
        return Response.ok().build();
    }

}
