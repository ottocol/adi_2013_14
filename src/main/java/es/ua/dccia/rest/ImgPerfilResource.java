/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.Tokens;
import es.ua.dccia.negocio.IImagenService;
import es.ua.dccia.negocio.ImagenService;
import es.ua.dccia.negocio.UsuarioBO;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author otto
 */
@Path("/usuarios/{login}/img_perfil")
public class ImgPerfilResource {
    
    @PUT
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed({Tokens.DEFAULT_USER_ROLE, Tokens.ADMIN_ROLE})
    public Response setImagenPerfil(@PathParam("login") String login, 
                                    @Context SecurityContext sc,
                                    @Context ServletContext servletContext,
                                    @Context UriInfo uriInfo,
                                    @FormDataParam("imagen") InputStream in,
                                    @FormDataParam("imagen") FormDataContentDisposition fileDetail) {
        if (!login.equals(sc.getUserPrincipal().getName()))
            return Response.status(Response.Status.FORBIDDEN).build();
        IImagenService imagenService = ImagenService.getInstance();
        imagenService.guardaFotoYThumbnail(in, IImagenService.PROFILE_FOLDER, login, IImagenService.THUMB_PROFILE_WIDTH);
        URI uri = uriInfo.getAbsolutePath();    
        return Response.created(uri).build();
    }
    
    @GET
    @Produces("image/jpg")
    public Response getImagenPerfil(@PathParam("login") String login, @Context ServletContext servletContext) {
        UsuarioBO usuarioBO = new UsuarioBO();
        if (usuarioBO.getUsuario(login)==null)
            throw new CustomNotFoundException("Usuario '" + login + "' no encontrado: ");
        File imag = new File(servletContext.getRealPath("/")+IImagenService.PROFILE_FOLDER+login+"_o."+IImagenService.DEFAULT_FORMAT);
        if (!imag.exists()) 
            throw new WebApplicationException("Error interno: la imagen de perfil no existe", Response.Status.INTERNAL_SERVER_ERROR); 
        return Response.ok(imag).build();
    }

    
}
