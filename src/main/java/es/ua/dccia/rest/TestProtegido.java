/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.rest;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author otto
 */
@Path("/protegido")
@RolesAllowed("registrado")
public class TestProtegido {
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)   
    public String HolaProtegido(@Context SecurityContext sc) {
        return "hola, soy un recurso protegido, eres " + sc.getUserPrincipal();
    }
}
