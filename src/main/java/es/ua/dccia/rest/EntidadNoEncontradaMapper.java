/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.rest;

import es.ua.dccia.EntidadNoEncontradaException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author otto
 */
@Provider
public class EntidadNoEncontradaMapper implements ExceptionMapper<EntidadNoEncontradaException> {
  @Override
  public Response toResponse(EntidadNoEncontradaException ex) {
    return Response.status(404).
      entity(ex.getMessage()).
      type("text/plain").
      build();
  }
}
