/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import es.ua.dccia.dominio.Usuario;
import javax.persistence.EntityManager;

/**
 *
 * @author otto
 */
public class UsuarioDao extends Dao<Usuario, String> {

    public UsuarioDao(EntityManager em) {
        super(em);
    }

    @Override
    public Usuario find(String login) {
        return this.em.find(Usuario.class, login);
    }
}
