/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import es.ua.dccia.dominio.Destinatario;
import es.ua.dccia.dominio.Peticion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author otto
 */
public class PeticionDao extends Dao<Peticion, Long> {

    public PeticionDao(EntityManager em) {
        super(em);
    }

    @Override
    public Peticion find(Long id) {
        Peticion p = em.find(Peticion.class, id);
        if (p!=null) {
            //recorremos los campos para que sean visibles
            p.getCreador();
            for (Destinatario d : p.getDestinatarios()) {
            }
        }
        return p;
    }

    public List<Peticion> findDestacadas(int cantidad) {
        Query queryDestacadas = em.createNamedQuery("Peticion.findDestacadas");
        queryDestacadas.setMaxResults(cantidad);
        List<Peticion> resultados = queryDestacadas.getResultList();
        return resultados;
    }
}
