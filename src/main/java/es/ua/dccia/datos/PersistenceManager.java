/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {
   static private final String PERSISTENCE_UNIT_NAME = "muevete";
   protected static PersistenceManager me = null;
   private EntityManagerFactory emf = null;

   private PersistenceManager() {
   }

   public static PersistenceManager getInstance() {
      if (me == null) {
         me = new PersistenceManager();
      }
      return me;
   }

   public void setEntityManagerFactory(EntityManagerFactory myEmf) {
      emf = myEmf;
   }

   public EntityManager createEntityManager() {
      if (emf == null) {
         emf = Persistence
               .createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
         this.setEntityManagerFactory(emf);
      }
      return emf.createEntityManager();
   }

   public void close() {
      if (emf != null)
         emf.close();
      emf = null;
   }
}