/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import es.ua.dccia.dominio.Destinatario;
import es.ua.dccia.dominio.Firma;
import es.ua.dccia.dominio.Peticion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author otto
 */
public class FirmaDao extends Dao<Firma, Long> {

    public FirmaDao(EntityManager em) {
        super(em);
    }

    @Override
    public Firma find(Long id) {
        Firma f = em.find(Firma.class, id);
        return f;
    }

}
