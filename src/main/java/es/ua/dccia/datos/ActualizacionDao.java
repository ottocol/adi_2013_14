/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.datos;

import es.ua.dccia.dominio.Actualizacion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author otto
 */
public class ActualizacionDao extends Dao<Actualizacion,Long> {
    public ActualizacionDao(EntityManager em) {
        super(em);
    }

    @Override
    public Actualizacion find(Long id) {
        Actualizacion a = em.find(Actualizacion.class, id);
        return a;
    }
    
    public List<Actualizacion> listarUltimas(long idPeticion, int cantidad) {
        Query q = em.createNamedQuery("Actualizacion.listarUltimas");
        q.setParameter("idPeticion", idPeticion);
        if (cantidad>0)
            q.setMaxResults(cantidad);
        return q.getResultList();
    }
  
}
