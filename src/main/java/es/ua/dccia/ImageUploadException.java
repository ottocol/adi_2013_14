package es.ua.dccia;

public class ImageUploadException extends RuntimeException {

	private static final long serialVersionUID = 5031567851825164899L;

	public ImageUploadException(String mensaje) {
		super(mensaje);
	}
	
	public ImageUploadException(Throwable causa) {
		super(causa);
	}

}
