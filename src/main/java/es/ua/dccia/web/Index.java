/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web;

import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.negocio.PeticionBO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author otto
 */
@WebServlet("/index")
public class Index extends MustacheServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PeticionBO pbo = new PeticionBO();
        Map m = new HashMap();
        List<Peticion> destacadas = pbo.listarDestacadas(5);
        m.put("destacadas", destacadas);
        render(m, "index", resp);
    }
    
    

}
