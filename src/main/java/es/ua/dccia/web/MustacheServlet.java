/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web;

import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.trimou.engine.MustacheEngine;

/**
 *
 * @author otto
 */
public class MustacheServlet extends HttpServlet {

    protected static MustacheEngine engine;
    
    
    public static void setMustacheEngine(MustacheEngine mustacheEngine) {
        engine = mustacheEngine;
    }
   
    public void render(Map m, String template, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        engine.getMustache(template).render(response.getWriter(),m);
    }   
    
}
