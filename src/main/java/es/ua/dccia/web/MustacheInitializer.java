/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.trimou.engine.MustacheEngine;
import org.trimou.engine.MustacheEngineBuilder;
import org.trimou.engine.config.EngineConfigurationKey;
import org.trimou.servlet.locator.ServletContextTemplateLocator;

/**
 *
 * @author otto
 */
@WebListener
public class MustacheInitializer implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        MustacheEngine engine = MustacheEngineBuilder
                .newBuilder()
                .setProperty(EngineConfigurationKey.DEBUG_MODE, true)
                .setProperty(EngineConfigurationKey.DEFAULT_FILE_ENCODING, "UTF-8")
                .addTemplateLocator(new ServletContextTemplateLocator(1,"/","html"))
                .build();
        MustacheServlet.setMustacheEngine(engine);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
    
}
