/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web;

import es.ua.dccia.MueveteException;
import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.dominio.Usuario;
import es.ua.dccia.negocio.PeticionBO;
import es.ua.dccia.negocio.UsuarioBO;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author otto
 */
@WebServlet("/usuario")
public class GetUsuario extends MustacheServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsuarioBO ubo = new UsuarioBO();
        String login = req.getParameter("login");
        if (login==null)
            throw new MueveteException("Falta el parámetro 'login'");
        else {  
            Usuario usuario = ubo.getUsuario(login);
            Map m = new HashMap();
            m.put("usuario", usuario);
            render(m, "usuario", resp);
        }
    }
}
