/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web.autenticacion;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author otto
 */
@WebServlet("/login")
public class Login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mensaje = "OK";  //por defecto todo ha ido bien
        int status = HttpServletResponse.SC_OK;
        
        String userName = request.getParameter("login");
        String password = request.getParameter("password");
        if (userName==null || ("").equals(userName) || (password==null) || ("").equals(password)) {
            mensaje = "Falta login y/o password";
            status = HttpServletResponse.SC_BAD_REQUEST;
        } else {
            try {
                if (request.getUserPrincipal()!=null) {
                    request.logout();
                }
                //Creamos una sesión porque si no existe no se guardará el login programático
                //Al menos ese es el comportamiento por defecto de Tomcat
                request.getSession();
                request.login(userName, password);
            }
            catch(ServletException se) {
                mensaje ="login y/o password incorrectos";
                status = HttpServletResponse.SC_FORBIDDEN;
            }
        }
        response.setStatus(status);
        response.getWriter().print(mensaje);
    }
}
