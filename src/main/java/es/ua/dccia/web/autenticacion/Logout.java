/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web.autenticacion;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author otto
 */
@WebServlet("/logout")
public class Logout extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException { 
        String mensaje = "OK";
        int status = HttpServletResponse.SC_OK;
        
        try {
            request.logout();
        } catch (ServletException se) {
            mensaje = "Error al hacer logout";
            status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        }
        response.setStatus(status);
        PrintWriter out = response.getWriter();
        out.print(mensaje);
        out.close();
    }
}
