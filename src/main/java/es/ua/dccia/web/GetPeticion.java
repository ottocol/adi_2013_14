/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.web;

import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.negocio.PeticionBO;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author otto
 */
@WebServlet("/peticion")
public class GetPeticion extends MustacheServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PeticionBO pbo = new PeticionBO();
        long id = Long.parseLong(req.getParameter("id"));
        Peticion peticion = pbo.getPeticion(id);
        Map m = new HashMap();
        m.put("peticion", peticion);
        render(m, "peticion", resp);
    }
}
