package es.ua.dccia;

public class AccesoNoPermitidoException extends RuntimeException {

    public AccesoNoPermitidoException(String mensaje) {
        super(mensaje);
    }

    public AccesoNoPermitidoException(Throwable causa) {
        super(causa);
    }

}
