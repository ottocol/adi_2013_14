/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import es.ua.dccia.dominio.Peticion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PeticionDaoTest {

    private static EntityManagerFactory emf;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;

    @BeforeClass
    public static void initDatabaseTest() throws ClassNotFoundException, SQLException, DatabaseUnitException {
        emf = Persistence.createEntityManagerFactory("muevete");
        /*
        Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        Connection jdbcConnection = (Connection) DriverManager
                .getConnection(
                "jdbc:derby:memory:unit-testing-jpa",
                "", "");
        */
         Class.forName("com.mysql.jdbc.Driver");
         Connection jdbcConnection = (Connection) DriverManager
               .getConnection(
                     "jdbc:mysql://localhost:3306/muevete_test",
                     "root", "adi");
        connection = new DatabaseConnection(jdbcConnection);

        FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
        flatXmlDataSetBuilder.setColumnSensing(true);
        dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("db/test-daos-dataset.xml"));
    }

    @AfterClass
    public static void closeEntityManagerFactory() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(connection, dataset);
        if (emf != null) {
            emf.close();
        }
    }

    @Before
    public void cleanDB() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
    }

    @Test
    public void testBuscarPeticiones() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        PeticionDao pdao = new PeticionDao(em);
        List<Peticion> destacadas = pdao.findDestacadas(5);
        assertEquals(2, destacadas.size());
        assertEquals(2, destacadas.get(0).getId());
        em.getTransaction().commit();
    }
}
