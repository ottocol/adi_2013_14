/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ua.dccia.datos;

import es.ua.dccia.dominio.Peticion;
import es.ua.dccia.dominio.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class UsuarioDaoTest {

    private static EntityManagerFactory emf;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;

    @BeforeClass
    public static void initDatabaseTest() throws ClassNotFoundException, SQLException, DatabaseUnitException {
        emf = Persistence.createEntityManagerFactory("muevete");
        Class.forName("com.mysql.jdbc.Driver");
        Connection jdbcConnection = (Connection) DriverManager
                .getConnection(
                "jdbc:mysql://localhost:3306/adi_test",
                "root", "adi");
        connection = new DatabaseConnection(jdbcConnection);

        FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
        flatXmlDataSetBuilder.setColumnSensing(true);
        dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("db/test-daos-dataset.xml"));
    }

    @AfterClass
    public static void closeEntityManagerFactory() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(connection, dataset);
        if (emf != null) {
            emf.close();
        }
    }

    @Before
    public void cleanDB() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
    }

    //@Test
    public void RegistroUsuario() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        UsuarioDao udao = new UsuarioDao(em);
        Usuario u = new Usuario();
        udao.create(u);
        em.getTransaction().commit();
        //Ahora lo buscamos a ver si está
        em = emf.createEntityManager();
        em.getTransaction().begin();
        udao = new UsuarioDao(em);
        Usuario encontrado = udao.find(u.getLogin());
        assertEquals(u, encontrado);
        em.getTransaction().commit();
    }
}
