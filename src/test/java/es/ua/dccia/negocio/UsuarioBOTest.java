/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ua.dccia.negocio;

import es.ua.dccia.datos.UsuarioDao;
import es.ua.dccia.dominio.Usuario;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author otto
 */
public class UsuarioBOTest {
    private static UsuarioBO usuarioBO;
    
    //@BeforeClass
    public static void init() {
        usuarioBO = new UsuarioBO();
    }
    
    //@Test
    public void testRegistrarUsuario() {
        Usuario usuario = new Usuario();
        usuario.setLogin("nuevo@ua.es");
        usuario.setPassword("secreto");
        usuario.setApellidos("Nuevo Nuevo");
        usuario.setNombre("Usu");
        usuario.setRol("registrado");
        usuarioBO.registrar(usuario);
        assertEquals(usuario, usuarioBO.getUsuario(usuario.getLogin()));
    }
    
}
