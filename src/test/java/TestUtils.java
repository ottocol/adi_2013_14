
import es.ua.dccia.utils.Utils;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author otto
 */
public class TestUtils {
    protected class MiClase1 {
        private int edad;
        private String nombre;

        public int getEdad() {
            return edad;
        }

        public void setEdad(int edad) {
            this.edad = edad;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
        
    }
    
    protected class MiClase2 {
        private int edad;
        private String nombre;
        private String apellidos;

        public int getEdad() {
            return edad;
        }

        public void setEdad(int edad) {
            this.edad = edad;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellidos() {
            return apellidos;
        }

        public void setApellidos(String apellidos) {
            this.apellidos = apellidos;
        }
        
        
    }
    
    //@Test
    public void testUtils() {
        MiClase1 mc1 = new MiClase1();
        MiClase2 mc2 = new MiClase2();
        mc1.setEdad(40);
        mc1.setNombre("Pepe");
        Utils.copyProperties(mc1, mc2, true);
        System.out.println(mc2.getEdad() + mc2.getNombre());
    }
    
}
